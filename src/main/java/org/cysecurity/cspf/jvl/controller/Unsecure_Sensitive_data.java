package org.cysecurity.cspf.jvl.controller;

import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;


import java.util.List;

class Unsecure_Sensitive_data{
	
	void execute() {
		List<Message> list = read();
		if (list != null && list.size() > 0) {
			String CreditCard = list.get(0).getBody();
			System.out.println(CreditCard);
		}
	}
	
	List<Message> read(){
		try{
			AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
			List<Message> messages = sqs.receiveMessage("queue145").getMessages();
			return messages;
		} catch (Exception ex){
			//
		}
		return null;
	}
}